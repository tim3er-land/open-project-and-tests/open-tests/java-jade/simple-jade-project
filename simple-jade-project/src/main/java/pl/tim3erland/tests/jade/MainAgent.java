package pl.tim3erland.tests.jade;

import jade.core.Agent;

/**
 * @author: Piotr Witowski
 * @date: 04.12.2021
 * @project simple-jade-projectv2
 * Day of week: sobota
 */
public class MainAgent extends Agent {

    @Override
    protected void setup() {
        System.out.println("Hello World. ");
        System.out.println("My name is " + getLocalName());
        addBehaviour(new SendTickerBehaviour(this, 2500));
    }


    @Override
    protected void takeDown() {
        System.out.println("Goodbey World. ");
    }


}
