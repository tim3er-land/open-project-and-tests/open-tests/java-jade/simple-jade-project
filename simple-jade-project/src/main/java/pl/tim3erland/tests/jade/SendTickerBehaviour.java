package pl.tim3erland.tests.jade;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * @author: Piotr Witowski
 * @date: 02.12.2021
 * @project simple-jade-projectv2
 * Day of week: czwartek
 */
public class SendTickerBehaviour extends TickerBehaviour {


    public SendTickerBehaviour(Agent a, long period) {
        super(a, period);
    }

    @Override
    protected void onTick() {
        System.out.println("send Message");
        sendMessage();
    }

    public void sendMessage() {
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.addReceiver(new AID("fredReceive", AID.ISLOCALNAME));
        msg.setLanguage("Polish");
        msg.setContent("Witaj");
        myAgent.send(msg);
    }
}
