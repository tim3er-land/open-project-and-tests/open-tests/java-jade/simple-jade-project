package pl.tim3erland.tests.jade;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * @author: Piotr Witowski
 * @date: 02.12.2021
 * @project simple-jade-projectv2
 * Day of week: czwartek
 */
public class ExampleTickerBehaviour extends TickerBehaviour {


    public ExampleTickerBehaviour(Agent a, long period) {
        super(a, period);
    }

    @Override
    protected void onTick() {
        ACLMessage msg = myAgent.receive();
        System.out.println(msg);
        if (msg != null && msg.getContent() != null && !msg.getContent().isEmpty()) {
            System.out.println(msg.getContent());
        } else {
            block();
        }
        System.out.println("TickerBehaviour");
    }
}
