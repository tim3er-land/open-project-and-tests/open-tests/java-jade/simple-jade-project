package pl.tim3erland.tests.jade;

import jade.core.Agent;

public class HelloAgent extends Agent {
    @Override
    protected void setup() {
        System.out.println("My name is " + getLocalName());
        addBehaviour(new ExampleTickerBehaviour(this, 1000));
    }


    @Override
    protected void takeDown() {
        System.out.println("Goodbey World. ");
    }

}
