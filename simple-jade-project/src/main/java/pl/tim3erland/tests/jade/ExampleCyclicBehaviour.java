package pl.tim3erland.tests.jade;

import jade.core.behaviours.CyclicBehaviour;

/**
 * @author: Piotr Witowski
 * @date: 02.12.2021
 * @project simple-jade-projectv2
 * Day of week: czwartek
 */
public class ExampleCyclicBehaviour extends CyclicBehaviour {

    @Override
    public void action() {
        System.out.println("CyclicBehaviour");
    }
}
